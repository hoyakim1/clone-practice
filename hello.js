function sayHello() {
  const animals = ['cat', 'dog', 'hamster']
  const food = [
    'ramen',
    'ramyun',
    'spaghetti',
    'udong',
  ]
  const colors = [
    'red',
    'blue',
    'green',
    'yellow'
  ]
  console.log("Hello, world!");
};

sayHello();
